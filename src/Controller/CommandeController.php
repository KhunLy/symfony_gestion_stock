<?php

namespace App\Controller;

use App\Entity\Commande;
use App\Entity\LigneCommande;
use App\Form\AddCommandeType;
use App\Repository\CommandeRepository;
use App\Repository\LigneCommandeRepository;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\Date;

class CommandeController extends AbstractController
{
    private $em;
    private $repo;

    public function __construct(CommandeRepository $repo,
                                EntityManagerInterface $em) {
        $this->em = $em;
        $this->repo = $repo;
    }

    #[Route('/commande', name: 'commande')]
    public function index(): Response
    {
        return $this->render('commande/index.html.twig', [
        ]);
    }

    #[Route('/commande/add', name: 'commande_add')]
    public function add(Request $request)
    {
        $new = new Commande();
        $form = $this->createForm(AddCommandeType::class, $new);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            //$ref = (new \DateTime())->format('ymd');
            $ref = date('ymd');
            $count = $this->repo->countByDate($ref) + 1;
            $ref .= str_pad($count, 4, '0', STR_PAD_LEFT);
            $new->setReference($ref);
            $new->setCreationDate(new \DateTime());
            $new->setUpdateDate(new \DateTime());
            $new->setEtat(0);
            $this->em->persist($new);
            $this->em->flush();
            $this->addFlash('success', 'OK');
            return $this->redirectToRoute('commande_edit', ['id' => $new->getId()]);
        }

        return $this->render('commande/add.html.twig', [
            'form' => $form->createView()
        ]);
    }
    #[Route('/commmande/edit/{id}',
        name: 'commande_edit',
        requirements: ['id' => '\d+'])]
    public function edit(Request $request, $id) {
        $commande = $this->repo->findWithClient($id);
        return $this->render('commande/edit.html.twig', [
            'commande' => $commande
        ]);
    }

    #[Route('/commande/addLine/{id}', name: 'commande_add_line')]
    public function ajouterLigne(
        $id,
        Request $request,
        ProduitRepository $pRepo,
        LigneCommandeRepository $lRepo,
    ) {
        // est-ce qu'il existe une commande liée à l'id?
        $commande = $this->repo->findWithLinesAndProducts($id);
        if($commande === null) {
            throw new NotFoundHttpException(); //404
        }
        if($commande->getEtat() !== 0) {
            throw new BadRequestHttpException();
        }

        $produitId = $request->request->get('produitId');
        $quantity = $request->request->get('quantity');

        // le produit a-t'il déjà été ajouté
        $ligne = $lRepo->findOneBy([
            'produit' => $produitId,
            'commande' => $id
        ]);

        if($ligne === null) {
            $ligne = new LigneCommande();
            $p = $pRepo->findOneBy([ 'id' => $produitId, 'deleted'=> false ]);
            if($p == null) {
                // code erreur 400
                throw new BadRequestHttpException();
            }
            $ligne->setProduit($p);
            $ligne->setQuantite($quantity);
            $ligne->setCommande($commande);
            $commande->addLigne($ligne);
            $this->em->persist($ligne);
            $this->em->flush();
        } else {
            $ligne->setQuantite($quantity);
            $this->em->flush();
        }

        return new JsonResponse(array_map(
            function($item) { return $item->serialize(); }
        , $commande->getLignes()->toArray()));
    }

    #[Route('commande/lines/{id}', name: 'commande_lines')]
    public function getLines($id) {
        $commande = $this->repo->findWithLinesAndProducts($id);

        if($commande === null)
            throw new NotFoundHttpException();

        return new JsonResponse(array_map(
            function($item) { return $item->serialize(); },
            $commande->getLignes()->toArray()));
    }
}
