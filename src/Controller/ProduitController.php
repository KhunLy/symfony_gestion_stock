<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Form\AddProduitType;
use App\Repository\ProduitRepository;
use Doctrine\DBAL\Schema\View;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProduitController extends AbstractController
{
    #[Route('/produit', name: 'produit')]
    public function index(ProduitRepository $repo): Response
    {
        $produits = $repo->findBy(['deleted' => false]);
        return $this->render('produit/index.html.twig', [
            'produits' => $produits,
        ]);
    }

    #[Route('/produit/add', name: 'produit_add')]
    public function add(Request $request, EntityManagerInterface $em, ProduitRepository $repo) {
        $newP = new Produit();
        $form = $this->createForm(AddProduitType::class, $newP);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $ref = strtoupper(substr($newP->getNom(), 0,4));
            $count = $repo->countByRef($ref) + 1;
            $newP->setReference($ref . str_pad($count, 4, '0', STR_PAD_LEFT));
            $newP->setDeleted(false);
            $em->persist($newP);
            $em->flush();
            $this->addFlash('success', 'OK');
            return $this->redirectToRoute('produit');
        }
        return $this->render('produit/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('produit/search', name: 'produit_search')]
    public function getByName(Request $request, ProduitRepository $repo)
    {
        $name = $request->query->get('name');
        $products = $repo->findByName($name);

        return new JsonResponse(
            array_map(
                function($item) { return $item->serialize(); },
                $products
            )
        );
    }
}
