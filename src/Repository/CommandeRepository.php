<?php

namespace App\Repository;

use App\Entity\Commande;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Commande|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commande|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commande[]    findAll()
 * @method Commande[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommandeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commande::class);
    }

    // /**
    //  * @return Commande[] Returns an array of Commande objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Commande
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function countByDate(string $ref)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->where('c.reference LIKE :p1');
        $qb->setParameter('p1', $ref . '%');
        $qb->select('COUNT(c.id)');
        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findWithClient($id)
    {
        // SELECT c.* FROM commande as c
        $qb = $this->createQueryBuilder('c');
        // WHERE id = :p1
        $qb->where('c.id = :p1');
        $qb->setParameter('p1', $id);
        // LEFT JOIN client as cl ON cl.id = c.clientId
        $qb->leftJoin('c.client', 'cl');
        //ajoute cl.* dans la clause select
        $qb->addSelect('cl');

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findWithLinesAndProducts($id)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->where('c.id = :p1');
        $qb->setParameter('p1', $id);
        $qb->leftJoin('c.lignes', 'l');
        $qb->leftJoin('l.produit', 'p');
        $qb->addSelect('l');
        $qb->addSelect('p');
        return $qb->getQuery()->getOneOrNullResult();
    }
}
